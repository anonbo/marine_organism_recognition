# 海洋生物识别

#### 介绍
海洋生物识别

#### 软件架构
android 9+ android studio 3.2(android-ndk-r16b-windows-x86_64) + darknet2ncnn


#### 安装教程

1.  下载程序包
2.  新建android studio 工程
3.  编译

#### 使用说明

1.  exp\oor\app\src\main\cpp C++源码，主要包含native-lib.cpp --- jni接口定义
2.  oor\app\src\main\jniLibs\armeabi-v7a 第三方支持库，主要为ncnn 和OpenCV
3.  \oor\app\src\main\java\bistu\grobot\oor andorid端程序
![输入图片说明](https://images.gitee.com/uploads/images/2019/1121/145222_355a642e_5058011.jpeg "微信图片_20191121144656.jpg")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
